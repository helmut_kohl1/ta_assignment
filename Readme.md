### Docker compose file to package FrontEnd, BackEnd and Cypress Tests.


## Build the docker images for all 3 containers

docker-compose build

## Run the containers 

docker-compose up

## Solution has attached gitlab-ci.yml file to run the CICD pipeline

